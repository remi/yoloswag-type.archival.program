import utils, requests

name = "discord"

def _print(str):
    print(f"[{name}] " + str)

def init(config):
    if not "webhookUrl" in config:
        _print("webhookUrl not found in config... why!")
        return False
    if not utils.test_server("https://discord.com/"):
        _print("failed to connect to discord api")
        return False
    _print("initialized!")
    return [config]

def query(data, date):
    return data

def download(data, tmpFolder, outFolder):
    pass

def postprocess(data, tmpFolder, outFolder):
    pass

def postsave(data, hash):
    requests.post(data[0]["webhookUrl"], json={'content': f'todays hash: {hash}'})