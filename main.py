import json, sys, importlib, utils, os, subprocess, shutil, time
from datetime import date

twodaysdate = date.today()
twodaystime = time.time()

print("~ yoloswag-type archival program ~")

print("initializing...")
if not utils.is_tool("ipfs"):
    print("[!] ipfs not found - this entire program is based around archiving stuff to ipfs, you might wanna install it")
    sys.exit(1)
if os.system("ipfs files ls /yoloswag > /dev/null 2>&1") != 0:
    print("[!] /yoloswag mfs folder not found... make it plz")
    sys.exit(1)
config = ""
with open("config.json") as config_file:
    try:
        config = config_file.read()
        config = json.loads(config)
    except:
        print("[!] config.json not found or invalid - please refer to config_example.json!!")
        sys.exit()
tmpFolder = "./temp"
outFolder = "./out"
if not utils.check_and_make_folder(tmpFolder): sys.exit()
if not utils.check_and_make_folder(outFolder): sys.exit()

plugins = {}
plugin_data = {}

for plugin_name, plugin_config in config.items():
    if plugin_config["enabled"]:
        plugins[plugin_name] = importlib.import_module(f"plugins.{plugin_name}")
        plugin_data[plugin_name] = plugins[plugin_name].init(plugin_config) # do stuff like "make sure the plugin works"
        if plugin_data[plugin_name] == False:
            print(f"[!] {plugin_name} failed to initialize - quitting")
            sys.exit()

print("querying new files...") # search for files we need to download. this is done before download step to prevent having one plugin take an hour to download stuff and tehn. you miss the 24hr window on other plugins
for plugin in plugins:
    plugin_data[plugin] = plugins[plugin].query(plugin_data[plugin], twodaystime)
    #query step cant "fail", at worst it returns Nothing
    #dont want the program to quit after we've actually started to grab stuff YK

print("downloading files...") # guess
for plugin in plugins:
    plugins[plugin].download(plugin_data[plugin], tmpFolder, outFolder)

print("processing downloaded files...") # stuff like "convert .m4a to .opus because .m4a is a fucking stupid format"
for plugin in plugins:
    plugins[plugin].postprocess(plugin_data[plugin], tmpFolder, outFolder)

print("saving all to ipfs...") # guess
hash = subprocess.check_output(f"ipfs add -prQ {outFolder}", shell=True, universal_newlines=True)
hash = hash.split("\n")[0]
os.system(f"ipfs files cp /ipfs/{hash} /yoloswag/{twodaysdate}.{twodaystime}")
os.system(f"ipfs pin rm -r {hash}") # dont wanna clog up the users pins

print("running post-save actions...") # basically - have a plugin automatically post the hash to your discord server, or w/e
for plugin in plugins:
    plugins[plugin].postsave(plugin_data[plugin], hash)

print("cleaning up...")
shutil.rmtree(tmpFolder)
shutil.rmtree(outFolder)

print(f"all done! your files are in /ipfs/{hash} and (mfs) /yoloswag/{twodaysdate}.{twodaystime}")