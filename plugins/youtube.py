# notes:
# you can detect ongoing livestreams by checking lengthSeconds == 0
# you'll have to decode the individous api to utf-8
# https://invidious.esmailelbob.xyz/api/v1/channels/UCK9V2B22uJYu3N7eR_BT9QA/

import utils, requests, shlex, os

name = "youtube"

def _print(str):
    print(f"[{name}] " + str)

def init(config):
    if not utils.is_tool("yt-dlp"):
        _print("yt-dlp not found - this is required for the youtube plugin")
        return False
    if not utils.is_tool("ffmpeg"):
        _print("ffmpeg not found - this is required for the youtube plugin")
        return False
    if not "quality" in config:
        _print("quality setting not found in config! (leaving it empty is fine, just dont delete it)")
        return False
    if not "audioFormat" in config:
        _print("audioFormat setting not found in config! (leaving it empty is fine, just dont delete it)")
        return False
    if not "invidousInstanceWithApiEnabled" in config:
        _print("invidousInstanceWithApiEnabled not found in config!")
        return False
    if not "channels" in config or not "channelsAudioOnly" in config:
        _print("channels/channelsAudioOnly array not found in config! please set either.... thats the point of this plugin...")
    if not utils.test_server(config["invidousInstanceWithApiEnabled"]):
        _print("failed to connect to invidous instance")
        return False
    _print("initialized!")
    return [config]

def query(data, date):
    data.insert(1, {})
    data.insert(2, {})
    print(date)
    for download in data[0]["channels"]:
        data = _queryActual(download, data, False, date)
    for download in data[0]["channelsAudioOnly"]:
        data = _queryActual(download, data, True, date)
    return data

def _queryActual(download, data, audioOnly, date):
    try:
        res = requests.get(f"{data[0]['invidousInstanceWithApiEnabled']}/api/v1/channels/{download}/")
        res = res.json()
        for video in res["latestVideos"]:
            if (date - 86400 <= video["published"]):
                if video["lengthSeconds"] != 0:
                    if res["author"] not in data[2 if audioOnly else 1]:
                        data[2 if audioOnly else 1][res["author"]] = []
                    data[2 if audioOnly else 1][res["author"]].append(video["videoId"])
    except:
        _print(f"epic fail querying {download}!")
    return data

def download(data, tmpFolder, outFolder):
    utils.check_and_make_folder(f"{outFolder}/{name}")
    for channel in data[1]:
        utils.check_and_make_folder(f"{outFolder}/{name}/{channel}", name)
        for video in data[1][channel]:
            _downloadActual(video, data, False, f"{outFolder}/{name}/{channel}")
    for channel in data[2]:
        utils.check_and_make_folder(f"{outFolder}/{name}/{channel}", name)
        for video in data[2][channel]:
            _downloadActual(video, data, True, f"{outFolder}/{name}/{channel}")
    
def _downloadActual(download, data, audioOnly, outFolder):
    command = "yt-dlp"
    if (audioOnly): command += " -x"
    if (data[0]["quality"] != ""): command += f" -f {data[0]['quality']} "
    if (audioOnly and data[0]["audioFormat"] != ""): command += f" --audio-format {data[0]['audioFormat']} "
    command += f"-P {shlex.quote(outFolder)} "
    command += download
    os.system(command)

def postprocess(data, tmpFolder, outFolder):
    pass # converting audio should be moved to here but mehh

def postsave(data, hash):
    pass