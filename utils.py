from shutil import which
import requests, os

def is_tool(name):
    return which(name) is not None

def test_server(srv):
    try:
        res = requests.get(srv)
        return res.status_code == 200
    except:
        return False

def check_and_make_folder(location, whoami = "!"):
    if os.path.isdir(location):
        print(f"[? {whoami}] {location} already exists - might wanna delete this?")
        return True
    else:
        try:
            os.mkdir(location)
        except:
            print(f"[{whoami}] failed to create {location}, do you have permission")
            return False
        return True