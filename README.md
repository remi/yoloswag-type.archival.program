# yoloswag-type.archival.program

offlines some things, i think

## todo

    [x] youtube plugin  
        [ ] exclude livestreams  
    [ ] rest of the plugins  
    [ ] idk more configuration probably !!!!!!!  

## what the hecky does this do?

yoloswag-type archival program is ... a program ... that archives the last 24 hours of web activity for you (potentially configurable, probably not =w=)

it then promptly throws that into a ipfs folder, and gives you the hash for it

it's useful if you dont like being online all the time, have intermittent connectivity (pro tip: toss it on a free vps and have it run every night, then download from there whenever you can), or just. like keeping things around

## why ipfs

it's kinda cool ok  
also built-in deduplication is useful in our case, since some plugins (read: sitedl) cant really check "has this content been made in the last 24 hours?" unlike. all the other built-in ones

## my original note for this program

* archive last 24hr of web activity
* throw in a ipfs folder
* plugin based
    * plugins have a few functions
    * init() (reads settings, tests connections, etc)
    * query() (figure out _what_ to download, as to prevent getting duplicate material over days because of long download jobs)
    * download() (guess)
    * postprocess() (converts stuff to other stuff)
* in order, you run init and query of all plugins, then download and postprocess of all plugins.

plugins that should probably exist

* youtube (piped/individous)
    * settings for what channels, exclude live streams
    * quality (desired)
    * audio only ??? (i mean this would be useful for like, whispaw)
* reddit (cringe, i know)
    * settings for what subreddits, filters (ideal for r/osugame -gameplay)
    * images only filter, but also make sure it can download entire comment sections and such. i wanna read my r/tfts
* soundcloud
    * heehee tunes
* pixiv
    * heehee lewds
* rss_download
    * for rss feeds that have download links (images maybe? because implementing images_only into rss_reader feels dumb)
* rss_torrent
    * for ur anime
* rss_reader
    * for ur ppy tweets
    * YES THIS REPLACES ANY POTENTIAL TWITTER PLUGINS JUST USE NITTER DAMMIT
* osu
    * oh yeah speaking of osu, download recently ranked/loved beatmaps
    * select da mode(s) too, ??? idk sort by tags maybe
* calendar
    * ical and such. combines them , , ,   and such,, for ideal , uhm,,,
    * (uni)
* sitedl
    * turns a site into pdf, turns a site into a single-html-file, turns a site into a reader mode, turns a site into a (recursive scan for dl, useful for sites that DONT SUCK like hisho)
    * useful for things that dont understand the concept of "rss"
    * also maybe has a depth function, so you can . go through the links, yk?
    * this one will totally duplicate stuff. like, a lot, but it's okay cause it's ipfs and deduplication is built in =w=
* github-releases
    * i mean it would be kinda nice to get fresh builds of lazer